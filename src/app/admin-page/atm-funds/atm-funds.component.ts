import { Component, OnInit } from '@angular/core';
import {BanknoteModel} from "../../models/banknote.model";
import {HttpAtmFundsService} from "../../http-services/http-atm-funds.service";

@Component({
  selector: 'app-atm-funds',
  templateUrl: './atm-funds.component.html',
  styleUrls: ['./atm-funds.component.css']
})
export class AtmFundsComponent implements OnInit {
  public banknotes: BanknoteModel[] = [];

  constructor(private httpAtmFundsService: HttpAtmFundsService) { }

  ngOnInit(): void {
    this.httpAtmFundsService.banknotesListShownChanged.subscribe(
      (banknotesList) => {
        this.banknotes = banknotesList;
      }
    )
    this.httpAtmFundsService.fetchBanknotes();
  }

}
