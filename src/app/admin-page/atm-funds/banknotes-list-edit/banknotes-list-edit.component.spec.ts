import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BanknotesListEditComponent } from './banknotes-list-edit.component';

describe('BanknotesListEditComponent', () => {
  let component: BanknotesListEditComponent;
  let fixture: ComponentFixture<BanknotesListEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BanknotesListEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BanknotesListEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
