import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpAtmFundsService} from "../../../http-services/http-atm-funds.service";
import {BanknoteTypeModel} from "../../../models/banknoteType.model";

@Component({
  selector: 'app-banknotes-list-edit',
  templateUrl: './banknotes-list-edit.component.html',
  styleUrls: ['./banknotes-list-edit.component.css']
})
export class BanknotesListEditComponent implements OnInit {
  banknoteCurrency: BanknoteTypeModel[] = [];
  banknotesListForm: FormGroup = new FormGroup({
    'banknotesList': new FormArray([], Validators.required)
  });

  constructor(private httpAtmFundsService: HttpAtmFundsService) { }

  ngOnInit(): void {
    this.httpAtmFundsService.fetchCurrency().subscribe(
      (response) => {
        this.banknoteCurrency = response;
      }
    );
  }

  public get banknotesListFormArray() {
    return this.banknotesListForm.get('banknotesList') as FormArray;
  }

  public getBanknoteTypeControlByGroupIndex(index: number) {
    return this.banknotesListFormArray.at(index).get('type') as FormControl;
  }

  public getBanknoteCountControlByGroupIndex(index: number) {
    return this.banknotesListFormArray.at(index).get('count') as FormControl;
  }

  public onRemoveNewBanknoteFromList(index: number): void {
    this.banknotesListFormArray.removeAt(index);
  }

  public onAddBanknoteToList(): void {
    (<FormArray>this.banknotesListForm.get('banknotesList')).push(
      new FormGroup({
        'type': new FormControl(null, Validators.required),
        'count': new FormControl(null, [Validators.required, Validators.pattern(/^[0-9]*$/)])
      })
    );
  }

  public clearForm(): void {
    while (this.banknotesListFormArray.length !== 0) {
      this.banknotesListFormArray.removeAt(0);
    }
  }

  public onSave(): void {
    this.httpAtmFundsService.saveBanknotesList(this.banknotesListFormArray.value);
    this.clearForm();
  }
}
