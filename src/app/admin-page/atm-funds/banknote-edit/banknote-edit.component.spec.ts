import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BanknoteEditComponent } from './banknote-edit.component';

describe('BanknoteEditComponent', () => {
  let component: BanknoteEditComponent;
  let fixture: ComponentFixture<BanknoteEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BanknoteEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BanknoteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
