import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpAtmFundsService} from "../../../http-services/http-atm-funds.service";
import {BanknoteTypeModel} from "../../../models/banknoteType.model";

@Component({
  selector: 'app-banknote-edit',
  templateUrl: './banknote-edit.component.html',
  styleUrls: ['./banknote-edit.component.css']
})
export class BanknoteEditComponent implements OnInit {
  public banknoteCurrency: BanknoteTypeModel[] = [];
  public banknoteForm: FormGroup  = new FormGroup({
    'type': new FormControl(null, Validators.required),
    'count': new FormControl(null, [Validators.required, Validators.pattern(/^[0-9]*$/)])
  });

  constructor(private httpAtmFundsService: HttpAtmFundsService) { }

  ngOnInit(): void {
    this.httpAtmFundsService.fetchCurrency().subscribe(
      (response) => {
        this.banknoteCurrency = response;
      }
    );
  }

  public get banknoteTypeControl() {
    return this.banknoteForm.get('type') as FormControl;
  }

  public onUpdate(): void {
    this.httpAtmFundsService.increaseBanknote(this.banknoteForm.value);
    this.banknoteForm.reset();
  }
}
