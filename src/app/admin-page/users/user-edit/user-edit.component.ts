import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpUsersService} from "../../../http-services/http-users.service";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  public userForm: FormGroup  = new FormGroup({
    'firstName': new FormControl(null, Validators.required),
    'lastName': new FormControl(null, Validators.required)
  });

  constructor(private httpUsersService: HttpUsersService) { }

  ngOnInit(): void {
  }

  public onSave(): void {
    this.httpUsersService.saveUser(this.userForm.value);
    this.userForm.reset();
  }

  public get firstNameControl() {
    return this.userForm.get('firstName') as FormControl;
  }

  public get lastNameControl() {
    return this.userForm.get('lastName') as FormControl;
  }
}
