import { Component, OnInit } from '@angular/core';
import {UserModel} from "../../models/user.model";
import {HttpUsersService} from "../../http-services/http-users.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public users: UserModel[] = [];

  constructor(private httpUsersService: HttpUsersService) { }

  ngOnInit(): void {
    this.httpUsersService.usersListShownChanged.subscribe(
      (usersListResponse) => {
        this.users = usersListResponse;
      }
    );
    this.httpUsersService.fetchUsers();
  }
}
