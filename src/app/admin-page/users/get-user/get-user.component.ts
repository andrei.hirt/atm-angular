import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpUsersService} from "../../../http-services/http-users.service";
import {UserModel} from "../../../models/user.model";
import {HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {DialogErrorComponent} from "../../../dialog-error/dialog-error.component";

@Component({
  selector: 'app-get-user',
  templateUrl: './get-user.component.html',
  styleUrls: ['./get-user.component.css']
})
export class GetUserComponent implements OnInit {
  public getUserForm: FormGroup  = new FormGroup({
    'userId': new FormControl(null, Validators.required),
  });
  public user: UserModel | undefined;
  public error: HttpErrorResponse | undefined;

  constructor(private httpUsersService: HttpUsersService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  get userIdControl() {
    return this.getUserForm.get('userId') as FormControl;
  }

  onGetUserById() {
    this.httpUsersService.getUser(this.userIdControl.value)
      .subscribe((response) => {
        this.user = response;
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });

    this.getUserForm.reset();
  }
}
