import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {
  navLinks: {path: string, label: string}[] = [
    { path: 'atm-funds', label: 'Manage ATM Funds' },
    { path: 'users', label: 'Manage Users' },
  ];
  activeLink: {path: string, label: string} = this.navLinks[0];

  constructor() { }

  ngOnInit(): void {
  }

}
