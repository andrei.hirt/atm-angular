export class BankAccountModel {
  constructor(public id: number, public iban: string, public holderName: string, public balance: number) {
  }
}
