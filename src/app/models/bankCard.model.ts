import {UserModel} from "./user.model";
import {BankAccountModel} from "./bankAccount.model";

export class BankCardModel {
  constructor(public id: number, public holderUser: UserModel, public cardNumber: string, public holderName: string,
              public expiryDate: string, public issuingBank: string, public cvv: string, public pin: string, public bankAccount: BankAccountModel) {
  }
}
