import {BankCardModel} from "./bankCard.model";

export class UserModel {
  constructor(public id: number, public firstName: string, public lastName: string, public bankCards: BankCardModel[]) {
  }
}
