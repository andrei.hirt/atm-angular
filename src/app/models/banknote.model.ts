import {BanknoteTypeModel} from "./banknoteType.model";

export class BanknoteModel {
  constructor(public type: BanknoteTypeModel, public count: number) {
  }
}

