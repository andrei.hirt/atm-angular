import { TestBed } from '@angular/core/testing';

import { HttpAtmFundsService } from './http-atm-funds.service';

describe('HttpAtmFundsService', () => {
  let service: HttpAtmFundsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpAtmFundsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
