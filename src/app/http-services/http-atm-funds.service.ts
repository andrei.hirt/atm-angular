import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {BanknoteModel} from "../models/banknote.model";
import {Subject} from "rxjs";
import {BanknoteTypeModel} from "../models/banknoteType.model";
import {MatDialog} from "@angular/material/dialog";
import {DialogErrorComponent} from "../dialog-error/dialog-error.component";

@Injectable({
  providedIn: 'root'
})
export class HttpAtmFundsService {
  public banknotesListShownChanged: Subject<BanknoteModel[]> = new Subject<BanknoteModel[]>();

  constructor(private http: HttpClient,
              private dialog: MatDialog) { }

  public fetchCurrency() {
    return this.http.get<BanknoteTypeModel[]>('/api/atm/currency');
  }

  public fetchBanknotes() {
    this.http.get<BanknoteModel[]>('/api/atm/funds')
      .subscribe((response) => {
        this.banknotesListShownChanged.next(response);
    });
  }

  public saveBanknotesList(banknotesList: BanknoteModel[]) {
    this.http.post<BanknoteModel[]>('/api/atm/funds/list', banknotesList)
      .subscribe((response) => {
        this.fetchBanknotes();
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });
  }

  public increaseBanknote(banknote: BanknoteModel) {
    this.http.put<BanknoteModel>('/api/atm/funds', banknote)
      .subscribe((response) => {
        this.fetchBanknotes();
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });
  }
}
