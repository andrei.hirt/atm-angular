import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {BanknoteModel} from "../models/banknote.model";

@Injectable({
  providedIn: 'root'
})
export class HttpTransactionsService {

  constructor(private http: HttpClient) { }

  public withdraw(bankCardId: number, amount: number) {
    return this.http.get<BanknoteModel[]>('/api/atm/withdraw/' + bankCardId, {
      params: new HttpParams().set('amount', amount)
    });
  }

  public deposit(bankCardId: number, amount: number) {
    return this.http.post<number>('/api/atm/deposit/' + bankCardId, null,{
      params: new HttpParams().set('amount', amount)
    });
  }

  public checkBalance(bankCardId: number) {
    return this.http.get<number>('/api/atm/check-balance/' + bankCardId);
  }
}
