import { TestBed } from '@angular/core/testing';

import { HttpBankCardsService } from './http-bank-cards.service';

describe('HttpBankCardsService', () => {
  let service: HttpBankCardsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpBankCardsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
