import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {UserModel} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class HttpUsersService {
  public usersListShownChanged: Subject<UserModel[]> = new Subject<UserModel[]>();

  constructor(private http: HttpClient) { }

  public fetchUsers() {
    this.http.get<UserModel[]>('/api/users')
      .subscribe((response) => {
        this.usersListShownChanged.next(response);
      });
  }

  public saveUser(user: UserModel) {
    this.http.post<UserModel>('/api/users', user)
      .subscribe((response) => {
        this.fetchUsers();
      });
  }

  public getUser(id: number) {
    return this.http.get<UserModel>('/api/users/' + id);
  }
}
