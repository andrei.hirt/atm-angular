import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {BankCardModel} from "../models/bankCard.model";
import {MatDialog} from "@angular/material/dialog";
import {DialogErrorComponent} from "../dialog-error/dialog-error.component";

@Injectable({
  providedIn: 'root'
})
export class HttpBankCardsService {
  public bankCardsListShownChanged: Subject<BankCardModel[]> = new Subject<BankCardModel[]>();
  public userId: number | undefined;

  constructor(private http: HttpClient,
              private dialog: MatDialog) { }

  public fetchBankCardsByUserId() {
    this.http.get<BankCardModel[]>('/api/users/' + this.userId + '/bank-cards')
      .subscribe((response) => {
        this.bankCardsListShownChanged.next(response);
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });
  }

  public saveBankCardForUserId(bankCard: BankCardModel) {
    this.http.post<BankCardModel>('/api/users/' + this.userId + '/bank-cards', bankCard)
      .subscribe((response) => {
        this.fetchBankCardsByUserId();
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });
  }

  public getBankCardByUserId(bankCardId: number) {
    return this.http.get<BankCardModel>('/api/users/' + this.userId + '/bank-cards/' + bankCardId);
  }
}
