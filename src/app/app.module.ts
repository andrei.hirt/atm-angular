import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { HomeComponent } from './home/home.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import { AtmFundsComponent } from './admin-page/atm-funds/atm-funds.component';
import {MatTabsModule} from "@angular/material/tabs";
import { UsersComponent } from './admin-page/users/users.component';
import { BanknoteEditComponent } from './admin-page/atm-funds/banknote-edit/banknote-edit.component';
import { BanknotesListEditComponent } from './admin-page/atm-funds/banknotes-list-edit/banknotes-list-edit.component';
import {MatSelectModule} from "@angular/material/select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {UserEditComponent} from './admin-page/users/user-edit/user-edit.component';
import {MatOptionModule} from "@angular/material/core";
import {GetUserComponent} from "./admin-page/users/get-user/get-user.component";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {MatListModule} from "@angular/material/list";
import { BankCardsComponent } from './user-page/bank-cards/bank-cards.component';
import { BankCardEditComponent } from './user-page/bank-cards/bank-card-edit/bank-card-edit.component';
import { GetBankCardComponent } from './user-page/bank-cards/get-bank-card/get-bank-card.component';
import { TransactionsComponent } from './user-page/transactions/transactions.component';
import { WithdrawComponent } from './user-page/transactions/withdraw/withdraw.component';
import { DepositComponent } from './user-page/transactions/deposit/deposit.component';
import { CheckBalanceComponent } from './user-page/transactions/check-balance/check-balance.component';
import {MatDialogModule} from "@angular/material/dialog";
import { DialogErrorComponent } from './dialog-error/dialog-error.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AdminPageComponent,
    UserPageComponent,
    HomeComponent,
    AtmFundsComponent,
    UsersComponent,
    BanknoteEditComponent,
    BanknotesListEditComponent,
    UserEditComponent,
    GetUserComponent,
    BankCardsComponent,
    BankCardEditComponent,
    GetBankCardComponent,
    TransactionsComponent,
    WithdrawComponent,
    DepositComponent,
    CheckBalanceComponent,
    DialogErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatTabsModule,
    MatSelectModule,
    MatOptionModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    HttpClientModule,
    FormsModule,
    ScrollingModule,
    MatListModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
