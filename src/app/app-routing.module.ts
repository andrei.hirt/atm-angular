import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdminPageComponent} from "./admin-page/admin-page.component";
import {UserPageComponent} from "./user-page/user-page.component";
import {HomeComponent} from "./home/home.component";
import {AtmFundsComponent} from "./admin-page/atm-funds/atm-funds.component";
import {UsersComponent} from "./admin-page/users/users.component";
import {BankCardsComponent} from "./user-page/bank-cards/bank-cards.component";
import {TransactionsComponent} from "./user-page/transactions/transactions.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'admin', component: AdminPageComponent, children: [
      {path: 'atm-funds', component: AtmFundsComponent},
      {path: 'users', component: UsersComponent}
    ]},
  {path: 'user', component: UserPageComponent, children: [
      {path: 'bank-cards', component: BankCardsComponent},
      {path: 'transactions', component: TransactionsComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
