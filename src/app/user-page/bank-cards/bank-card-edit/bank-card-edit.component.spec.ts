import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankCardEditComponent } from './bank-card-edit.component';

describe('BankCardEditComponent', () => {
  let component: BankCardEditComponent;
  let fixture: ComponentFixture<BankCardEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankCardEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BankCardEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
