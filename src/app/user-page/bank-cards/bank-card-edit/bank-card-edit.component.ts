import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpBankCardsService} from "../../../http-services/http-bank-cards.service";

@Component({
  selector: 'app-bank-card-edit',
  templateUrl: './bank-card-edit.component.html',
  styleUrls: ['./bank-card-edit.component.css']
})
export class BankCardEditComponent implements OnInit {
  public bankCardForm: FormGroup  = new FormGroup({
    'cardNumber': new FormControl(null, [Validators.required, Validators.pattern(/^[0-9]{16}$/)]),
    'holderName': new FormControl(null, Validators.required),
    'expiryDate': new FormControl(null, Validators.required),
    'issuingBank': new FormControl(null, Validators.required),
    'cvv': new FormControl(null, [Validators.required, Validators.pattern(/^[0-9]{3}$/)]),
    'pin': new FormControl(null, [Validators.required, Validators.pattern(/^[0-9]{4}$/)])
  });
  public bankAccountForm: FormGroup  = new FormGroup({
    'iban': new FormControl(null, Validators.required),
    'holderName': new FormControl(null, Validators.required),
    'balance': new FormControl(null, [Validators.required, Validators.pattern(/^[0-9]*$/)])
  });

  constructor(private httpBankCardsService: HttpBankCardsService) { }

  ngOnInit(): void {
    this.bankCardForm.addControl('bankAccount', this.bankAccountForm);
  }

  onSave() {
    this.httpBankCardsService.saveBankCardForUserId(this.bankCardForm.value);
    this.bankCardForm.reset();
    this.bankAccountForm.reset();
  }

  public getBankCardControlByName(formControlName: string) {
    return this.bankCardForm.get(formControlName) as FormControl;
  }

  public getBankAccountControlByName(formControlName: string) {
    return this.bankAccountForm.get(formControlName) as FormControl;
  }
}
