import { Component, OnInit } from '@angular/core';
import {BankCardModel} from "../../models/bankCard.model";
import {HttpBankCardsService} from "../../http-services/http-bank-cards.service";

@Component({
  selector: 'app-bank-cards',
  templateUrl: './bank-cards.component.html',
  styleUrls: ['./bank-cards.component.css']
})
export class BankCardsComponent implements OnInit {
  public bankCards: BankCardModel[] = [];

  constructor(private httpBankCardsService: HttpBankCardsService) { }

  ngOnInit(): void {
    this.httpBankCardsService.bankCardsListShownChanged.subscribe(
      (bankCardsListResponse) => {
        this.bankCards = bankCardsListResponse;
      }
    );

    this.httpBankCardsService.fetchBankCardsByUserId();
  }
}
