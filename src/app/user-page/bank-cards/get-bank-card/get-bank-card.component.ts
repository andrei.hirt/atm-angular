import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BankCardModel} from "../../../models/bankCard.model";
import {HttpBankCardsService} from "../../../http-services/http-bank-cards.service";
import {MatDialog} from "@angular/material/dialog";
import {HttpErrorResponse} from "@angular/common/http";
import {DialogErrorComponent} from "../../../dialog-error/dialog-error.component";

@Component({
  selector: 'app-get-bank-card',
  templateUrl: './get-bank-card.component.html',
  styleUrls: ['./get-bank-card.component.css']
})
export class GetBankCardComponent implements OnInit {
  public getBankCardForm: FormGroup  = new FormGroup({
    'bankCardId': new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
  });
  public bankCard: BankCardModel | undefined;

  constructor(private httpBankCardsService: HttpBankCardsService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  public onGetBankCardById(): void {
    this.httpBankCardsService.getBankCardByUserId(this.bankCardIdControl.value)
      .subscribe((response) => {
        this.bankCard = response;
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });

    this.getBankCardForm.reset();
  }

  get bankCardIdControl() {
    return this.getBankCardForm.get('bankCardId') as FormControl;
  }
}
