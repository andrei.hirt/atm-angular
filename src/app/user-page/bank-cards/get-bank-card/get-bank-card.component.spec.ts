import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBankCardComponent } from './get-bank-card.component';

describe('GetBankCardComponent', () => {
  let component: GetBankCardComponent;
  let fixture: ComponentFixture<GetBankCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetBankCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetBankCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
