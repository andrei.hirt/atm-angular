import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {UserModel} from "../models/user.model";
import {HttpUsersService} from "../http-services/http-users.service";
import {HttpBankCardsService} from "../http-services/http-bank-cards.service";
import {HttpErrorResponse} from "@angular/common/http";
import {DialogErrorComponent} from "../dialog-error/dialog-error.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  public navLinks: {path: string, label: string}[] = [
    { path: 'bank-cards', label: 'Manage BankCards' },
    { path: 'transactions', label: 'Perform Transactions' }
  ];
  public activeLink: {path: string, label: string} = this.navLinks[0];
  public userIdFormControl: FormControl = new FormControl(null, Validators.required);
  public userLoggedIn: UserModel | undefined;

  constructor(private httpUsersService: HttpUsersService,
              private httpBankCardsService: HttpBankCardsService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
  }


  public onLogIn(): void {
    this.httpUsersService.getUser(this.userIdFormControl.value)
      .subscribe((response) => {
        this.userLoggedIn = response;
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });

    this.httpBankCardsService.userId = this.userIdFormControl.value;
  }
}
