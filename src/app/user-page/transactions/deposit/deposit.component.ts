import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpTransactionsService} from "../../../http-services/http-transactions.service";
import {MatDialog} from "@angular/material/dialog";
import {HttpErrorResponse} from "@angular/common/http";
import {DialogErrorComponent} from "../../../dialog-error/dialog-error.component";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {
  public depositForm: FormGroup  = new FormGroup({
    'bankCardId': new FormControl(null, Validators.required),
    'amount': new FormControl(null, Validators.required)
  });
  public amountForDeposit: number | undefined;

  constructor(private httpTransactionsService: HttpTransactionsService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  public onDeposit(): void {
    this.httpTransactionsService.deposit(this.bankCardIdControl.value, this.amountControl.value).subscribe(
      (response) => {
        this.amountForDeposit = response;
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });

    this.depositForm.reset();
  }

  public get bankCardIdControl() {
    return this.depositForm.get('bankCardId') as FormControl;
  }

  public get amountControl() {
    return this.depositForm.get('amount') as FormControl;
  }
}
