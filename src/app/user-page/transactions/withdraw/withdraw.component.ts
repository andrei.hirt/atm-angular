import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpTransactionsService} from "../../../http-services/http-transactions.service";
import {BanknoteModel} from "../../../models/banknote.model";
import {MatDialog} from "@angular/material/dialog";
import {HttpErrorResponse} from "@angular/common/http";
import {DialogErrorComponent} from "../../../dialog-error/dialog-error.component";

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
  public withdrawForm: FormGroup  = new FormGroup({
    'bankCardId': new FormControl(null, Validators.required),
    'amount': new FormControl(null, Validators.required)
  });
  public banknotesFromWithdraw: BanknoteModel[] | undefined;

  constructor(private httpTransactionsService: HttpTransactionsService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  public onWithdraw(): void {
    this.httpTransactionsService.withdraw(this.bankCardIdControl.value, this.amountControl.value).subscribe(
      (response) => {
        this.banknotesFromWithdraw = response;
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });

    this.withdrawForm.reset();
  }

  public get bankCardIdControl() {
    return this.withdrawForm.get('bankCardId') as FormControl;
  }

  public get amountControl() {
    return this.withdrawForm.get('amount') as FormControl;
  }
}
