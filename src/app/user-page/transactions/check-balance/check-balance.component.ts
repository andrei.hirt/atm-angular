import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpTransactionsService} from "../../../http-services/http-transactions.service";
import {MatDialog} from "@angular/material/dialog";
import {HttpErrorResponse} from "@angular/common/http";
import {DialogErrorComponent} from "../../../dialog-error/dialog-error.component";

@Component({
  selector: 'app-check-balance',
  templateUrl: './check-balance.component.html',
  styleUrls: ['./check-balance.component.css']
})
export class CheckBalanceComponent implements OnInit {
  public checkBalanceForm: FormGroup  = new FormGroup({
    'bankCardId': new FormControl(null, Validators.required),
  });
  public balance: number | undefined;

  constructor(private httpTransactionsService: HttpTransactionsService,
              private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  public onCheckBalance(): void {
    this.httpTransactionsService.checkBalance(this.bankCardIdControl.value).subscribe(
      (response) => {
        this.balance = response;
      }, (error: HttpErrorResponse) => {
        this.dialog.open(DialogErrorComponent, {
          disableClose: true,
          data: error
        });
      });

    this.checkBalanceForm.reset();
  }

  get bankCardIdControl() {
    return this.checkBalanceForm.get('bankCardId') as FormControl;
  }
}
